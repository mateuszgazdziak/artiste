import {
  Module,
  MiddlewareConsumer,
  NestModule,
} from '@nestjs/common';
import { ArtistModule } from './artist/artist.module';
import { Proxy } from './middleware/ProxyMiddleware';
@Module({
  imports: [ ArtistModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(Proxy).forRoutes('artists');
  }
}
