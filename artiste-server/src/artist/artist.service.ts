import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { Artist } from './artist.controller';

const SERVER_ROOT = 'rest.bandsintown.com';
const API_ROOT = `http://${SERVER_ROOT}`;
const APP_ID = 1;

@Injectable()
export class ArtistService {
  async getArtistDetails(name): Promise<Artist> {
    const artistDetails = await axios.get(
      `${API_ROOT}/artists/${name}?app_id=${APP_ID}`,
    );

    const artistDetailsData =
      artistDetails.status === 200 && artistDetails.data;

    if (!artistDetailsData) {
      throw new Error(`Artist: ${name} not found`);
    } else {
      const artistEvents = await axios.get(
        `${API_ROOT}/artists/${name}/events?app_id=${APP_ID}`,
      );

      return {
        artistName: artistDetailsData.name,
        artistFacebook: artistDetailsData.facebook_page_url,
        artistPicture: artistDetailsData.image_url,
        artistEvents:
          artistEvents.data &&
          artistEvents.data.map(event => ({
            eventVenue: event.venue.name,
            eventCountry: event.venue.country,
            eventDate: event.datetime,
          })),
      };
    }
  }
}
