import { CacheModule, Module } from '@nestjs/common';
import { ArtistController } from './artist.controller';
import { ArtistService } from './artist.service';

@Module({
  imports: [CacheModule.register({ ttl: 15 * 60 })],
  controllers: [ArtistController],
  providers: [ArtistService]
})
export class ArtistModule {}
