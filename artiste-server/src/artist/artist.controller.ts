import {
  Controller,
  Get,
  Param,
  UseInterceptors,
  CacheInterceptor,
} from '@nestjs/common';
import { ArtistService } from './artist.service';

type Event = {
  eventVenue: string;
  eventCountry: string;
  eventDate: string;
};
export type Artist = {
  artistName: string;
  artistPicture: string;
  artistFacebook: string;
  artistEvents: Event[];
};

@Controller('artist')
@UseInterceptors(CacheInterceptor)
export class ArtistController {
  constructor(private readonly artistService: ArtistService) {}

  @Get(':name')
  async findOne(@Param() params): Promise<Artist> {
    return await this.artistService.getArtistDetails(params.name);
  }
}
