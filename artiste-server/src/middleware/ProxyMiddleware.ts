import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class Proxy implements NestMiddleware {
  use(req: Request, res: Response, next: Function) {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  }
}
