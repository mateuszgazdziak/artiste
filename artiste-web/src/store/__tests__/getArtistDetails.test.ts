import { thunkGetArtist } from "../artist/thunks";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { apiCall } from "api";
import {
  getArtistFailure,
  getArtistRequest,
  getArtistSuccess
} from "../artist/actions";

const mockStore = configureStore([thunk]);
export const createMockStore = (state = {}) => {
  return mockStore({ ...state });
};

let store;

describe("thunkGetArtist", () => {
  beforeEach(() => {
    store = createMockStore({ artists: [] });
  });
  test("successful request", () => {
    const fakeResponse = { artistName: "toool" };

    jest.spyOn(apiCall, "getArtistDetails").mockImplementation(() => {
      return Promise.resolve(fakeResponse);
    });

    store.dispatch(thunkGetArtist("tool")).then(() => {
      const actionsCalled = store.getActions();
      expect(actionsCalled).toEqual([
        getArtistRequest("tool"),
        getArtistSuccess("tool", fakeResponse)
      ]);
    });
  });

  test("failure request", () => {
    const fakeResponse = { error: "terror" };

    jest.spyOn(apiCall, "getArtistDetails").mockImplementation(() => {
      return Promise.reject(fakeResponse);
    });

    store.dispatch(thunkGetArtist("tool")).then(() => {
      const actionsCalled = store.getActions();
      expect(actionsCalled).toEqual([
        getArtistRequest("tool"),
        getArtistFailure("tool", fakeResponse)
      ]);
    });
  });
});
