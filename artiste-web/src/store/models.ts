export type Event = {
  eventVenue: string;
  eventCountry: string;
  eventDate: string;
};

export type Artist = {
  artistName: string;
  artistPicture: string;
  artistFacebook: string;
  artistEvents: Event[];
};
