import { Artist } from "../models";

export enum ArtistActionNames {
  FETCH_ARTIST_REQUEST = "@artist/FETCH_REQUEST",
  FETCH_ARTIST_SUCCESS = "@artist/FETCH_SUCCESS",
  FETCH_ARTIST_ERROR = "@artist/FETCH_ERROR"
}

type successPayload = {
  artistName: string;
  artistDetails: Artist;
};

type failurePayload = {
  artistName: string;
  error: any;
};

type FetchArtistRequest = {
  type: ArtistActionNames.FETCH_ARTIST_REQUEST;
  payload: string;
};

type FetchArtistSuccess = {
  type: ArtistActionNames.FETCH_ARTIST_SUCCESS;
  payload: successPayload;
};

type FetchArtistFailure = {
  type: ArtistActionNames.FETCH_ARTIST_ERROR;
  payload: failurePayload;
};

export type ArtistActionTypes =
  | FetchArtistRequest
  | FetchArtistSuccess
  | FetchArtistFailure;
