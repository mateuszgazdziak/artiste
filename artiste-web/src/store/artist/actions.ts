import { Artist } from "../models";
import { ArtistActionNames } from "./types";
export const getArtistRequest = (name: string) => ({
  type: ArtistActionNames.FETCH_ARTIST_REQUEST,
  payload: name
});

export const getArtistSuccess = (
  artistName: string,
  artistDetails: Artist
) => ({
  type: ArtistActionNames.FETCH_ARTIST_SUCCESS,
  payload: { artistName, artistDetails }
});

export const getArtistFailure = (artistName: string, error: any) => ({
  type: ArtistActionNames.FETCH_ARTIST_ERROR,
  payload: { artistName, error }
});
