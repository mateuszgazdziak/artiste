import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { AppState } from "store";
import {
  getArtistFailure,
  getArtistRequest,
  getArtistSuccess
} from "store/artist/actions";

import { apiCall } from "api";

export const thunkGetArtist = (
  artistName: string
): ThunkAction<void, AppState, null, Action<string>> => async dispatch => {
  dispatch(getArtistRequest(artistName));

  return apiCall
    .getArtistDetails(artistName)
    .then(artistDetails => {
      dispatch(getArtistSuccess(artistName, artistDetails));
    })
    .catch(error => dispatch(getArtistFailure(artistName, error)));
};
