import { ArtistActionNames, ArtistActionTypes } from "./types";

export const defaultArtistsState = () => ({});

export interface ArtistsState {
  [key: string]: any;
}

export const artistsReducer = (
  state: ArtistsState = defaultArtistsState(),
  action: ArtistActionTypes
): ArtistsState => {
  switch (action.type) {
    case ArtistActionNames.FETCH_ARTIST_REQUEST: {
      return {
        ...state,
        [action.payload]: { loading: true, error: false, details: {} }
      };
    }

    case ArtistActionNames.FETCH_ARTIST_SUCCESS: {
      return {
        ...state,
        [action.payload.artistName]: {
          details: action.payload.artistDetails,
          loading: false,
          error: false
        }
      };
    }

    case ArtistActionNames.FETCH_ARTIST_ERROR: {
      return {
        ...state,
        [action.payload.artistName]: {
          ...state[action.payload.artistName],
          loading: false,
          error: true
        }
      };
    }
    default:
      return state;
  }
};
