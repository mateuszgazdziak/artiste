import React from "react";

type Props = {
  label?: string;
  symbol: string;
};

const Emoji: React.FC<Props> = (props: Props) => (
  <span
    role="img"
    aria-label={props.label ? props.label : ""}
    aria-hidden={props.label ? "false" : "true"}
  >
    {props.symbol}
  </span>
);
export default Emoji;
