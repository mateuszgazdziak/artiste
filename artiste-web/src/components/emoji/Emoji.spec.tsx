import React from "react";
import { create } from "react-test-renderer";
import Emoji from ".";

describe("Emoji component", () => {
  test("Matches the snapshot without label", () => {
    const emoji = create(<Emoji symbol="❤" />);
    expect(emoji.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with label", () => {
    const emoji = create(<Emoji symbol="❤" label="label" />);
    expect(emoji.toJSON()).toMatchSnapshot();
  });
});
