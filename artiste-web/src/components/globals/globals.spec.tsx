import React from "react";
import { create } from "react-test-renderer";
import { transformToRGBA, FlexCol, FlexRow, H1, H2, H3 } from ".";

describe("transformToRGBA", () => {
  test("", () => {
    expect(transformToRGBA("#FFFFFF")).toEqual("rgb(255,255,255)");
    expect(transformToRGBA("#FFF")).toEqual("rgb(255,255,255)");
  });
});

describe("H1 component", () => {
  test("Matches the snapshot", () => {
    const h1 = create(<H1 />);
    expect(h1.toJSON()).toMatchSnapshot();
  });
});

describe("H2 component", () => {
  test("Matches the snapshot", () => {
    const h2 = create(<H2 />);
    expect(h2.toJSON()).toMatchSnapshot();
  });
});

describe("H3 component", () => {
  test("Matches the snapshot", () => {
    const h3 = create(<H3 />);
    expect(h3.toJSON()).toMatchSnapshot();
  });
});

describe("FlexCol component", () => {
  test("Matches the snapshot", () => {
    const flexCol = create(<FlexCol />);
    expect(flexCol.toJSON()).toMatchSnapshot();
  });
});

describe("FlexRow component", () => {
  test("Matches the snapshot", () => {
    const flexRow = create(<FlexRow />);
    expect(flexRow.toJSON()).toMatchSnapshot();
  });
});
