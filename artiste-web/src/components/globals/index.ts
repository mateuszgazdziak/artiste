import styled, { css } from "styled-components";
import theme from "ui/theme";
import { MEDIA_BREAK } from "ui/layout";
export const fontStack = css`
  font-family: -apple-system, BlinkMacSystemFont, "Helvetica", "Segoe",
    sans-serif;
`;

/**
 * Returns hex converted to rgb()/rgba() format
 *
 * @param hex
 * @param alpha
 */

export const transformToRGBA = (hex: string, alpha?: number) => {
  let r;
  let g;
  let b;

  if (hex.length === 4) {
    const hexValues = hex.split("#")[1];
    r = parseInt(hexValues[0] + hexValues[0], 16);
    g = parseInt(hexValues[1] + hexValues[1], 16);
    b = parseInt(hexValues[2] + hexValues[2], 16);
  } else {
    r = parseInt(hex.slice(1, 3), 16);
    g = parseInt(hex.slice(3, 5), 16);
    b = parseInt(hex.slice(5, 7), 16);
  }

  return alpha ? `rgb(${r},${g},${b},${alpha})` : `rgb(${r},${g},${b})`;
};

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
`;

export const H1 = styled.h1`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 1.5rem;
  line-height: 1.3;
  margin: 0;
  padding: 0;
`;

export const H2 = styled.h2`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 32px;
  line-height: 1.3;
  margin: 0;
  padding: 4px;
`;

export const H3 = styled.h3`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 24px;
  line-height: 1.3;
  margin: 0;
  padding: 0;
`;

export const SvgWrapper = styled.div`
  position: absolute;
  flex: none;
  z-index: 1;
  height: 80px;
  width: 110%;
  bottom: -4px;
  left: -5%;
  right: -5%;
  display: inline-block;
  pointer-events: none;

  @media (max-width: ${MEDIA_BREAK}px) {
    width: 150%;
    left: -25%;
    right: -25%;
  }
`;
