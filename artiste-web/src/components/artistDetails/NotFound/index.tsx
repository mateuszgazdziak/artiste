import React from "react";
import { StyledLink } from "./style";
import styled from "styled-components";
import { H2 } from "components/globals";

const ArtistNotFound: React.FC = props => {
  return (
    <>
      <span>
        <H2>Artist not found</H2>
      </span>
      <br />
      <span>
        Click <StyledLink to="/">here</StyledLink> to go back
      </span>
    </>
  );
};

const StyledArtistNotFound = styled(ArtistNotFound)`
  span {
    display: block;
  }
`;
export default StyledArtistNotFound;
