import styled from "styled-components";
import { Link } from "react-router-dom";
import theme from "ui/theme";

export const StyledLink = styled(Link)`
  cursor: pointer;
  color: ${theme.text.secondary};
`;
