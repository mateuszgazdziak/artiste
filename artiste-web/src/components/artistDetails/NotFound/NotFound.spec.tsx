import React from "react";
import { create } from "react-test-renderer";
import ArtistNotFound from ".";
import { BrowserRouter } from "react-router-dom";

describe("ArtistNotFound component", () => {
  test("Matches the snapshot without label", () => {
    const notFound = create(
      <BrowserRouter>
        <ArtistNotFound />
      </BrowserRouter>
    );
    expect(notFound.toJSON()).toMatchSnapshot();
  });
});
