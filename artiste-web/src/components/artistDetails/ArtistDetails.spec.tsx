import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { ArtistDetails } from "./ArtistDetailsComponent";
import { thunkGetArtist } from "store/artist/thunks";

import { BrowserRouter } from "react-router-dom";

let container;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});
afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("ArtistDetails component", () => {
  test("it shows artists name if provided in details", async () => {
    const artistDetails = {
      artistName: "Tool",
      artistPicture: "toolImg"
    };

    await act(async () => {
      render(
        <ArtistDetails
          thunkGetArtist={thunkGetArtist}
          isLoading={false}
          artistName="tool"
          artistDetails={artistDetails}
        />,
        container
      );
    });
    expect(container.textContent).toBe("ToolThere are no upcomming concerts");
  });

  test("it shows no artist found if there is error", async () => {
    const artistDetails = {
      artistName: "Tool",
      artistPicture: "toolImg"
    };

    await act(async () => {
      render(
        <BrowserRouter>
          <ArtistDetails
            thunkGetArtist={thunkGetArtist}
            isLoading={false}
            error="true"
            artistDetails={artistDetails}
          />
        </BrowserRouter>,
        container
      );
    });
    expect(container.textContent).toBe("Artist not foundClick here to go back");
  });
});
