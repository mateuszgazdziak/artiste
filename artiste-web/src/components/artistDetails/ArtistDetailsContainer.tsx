import { connect } from "react-redux";
import { ArtistDetails } from "components/artistDetails/ArtistDetailsComponent";
import { ArtistsState } from "store/artist/reducers";
import { thunkGetArtist } from "store/artist/thunks";
import { RouteComponentProps } from "react-router";

type Props = {
  artistName?: string;
};

const mapStateToProps = (
  state: ArtistsState,
  ownProps: RouteComponentProps<Props>
) => {
  const {
    match: {
      params: { artistName }
    }
  } = ownProps;

  const artistState = artistName && state.artists[artistName];


  return {
    artistName: artistName || "",
    artistDetails: artistState &&  artistState.details,
    isLoading: artistState && artistState.loading,
    error: artistState && artistState.error
  };
};

export default connect(
  mapStateToProps,
  { thunkGetArtist }
)(ArtistDetails);
