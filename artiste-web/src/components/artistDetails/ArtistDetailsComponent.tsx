import React, { useEffect, useState } from "react";
import { Artist } from "../../store/models";
import ArtistNotFound from "./NotFound";
import Loader from "components/loader";
import ArtistOverview from "./Overview";

type Props = {
  artistName: string;
  thunkGetArtist: any;
  artistDetails: Artist;
  isLoading: boolean;
  error: any;
};

export const ArtistDetails: React.FC<Props> = (props: Props) => {
  const { artistName, thunkGetArtist, artistDetails, isLoading, error } = props;
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      await thunkGetArtist(artistName);
    };

    if (error) {
      setIsError(true);
    }
    if (artistName && !artistDetails) {
      fetchData();
    }
  }, [artistName, artistDetails, thunkGetArtist, isLoading, error]);

  const renderContent = () => {
    if (isError) {
      return <ArtistNotFound />;
    }
    return isLoading ? <Loader /> : renderArtistInfo();
  };

  const renderArtistInfo = () => {
    return (
      artistDetails && (
        <ArtistOverview
          artistName={artistDetails.artistName}
          artistFacebook={artistDetails.artistFacebook}
          artistPicture={artistDetails.artistPicture}
          events={artistDetails.artistEvents}
        />
      )
    );
  };

  return <>{renderContent()}</>;
};
