import ArtistDetails from "components/artistDetails/ArtistDetailsContainer";
import { withRouter } from "react-router-dom";

export default withRouter(ArtistDetails);
