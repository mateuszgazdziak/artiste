import React from "react";
import { H2, H3 } from "components/globals";
import { IMG } from "./style";
import { Event } from "../../../store/models";
import Facebook from "components/facebook";
import { StyledFlex } from "ui/views/style";
import EventList from "components/eventList";

type Props = {
  artistName: string;
  artistPicture?: string;
  artistFacebook?: string;
  events?: Event[];
};

const ArtistOverview: React.FC<Props> = (props: Props) => {
  const renderFB = () => {
    return props.artistFacebook ? (
      <a href={props.artistFacebook}>
        <Facebook />
      </a>
    ) : null;
  };

  const renderEvents = () => {
    return props.events && props.events.length ? (
      <>
        <H3>Upcomming concerts:</H3>
        <EventList events={props.events} />
      </>
    ) : (
      <H3>There are no upcomming concerts</H3>
    );
  };

  return (
    <StyledFlex>
      {props.artistPicture ? <IMG src={props.artistPicture} /> : null}
      <H2>{props.artistName}</H2><br />
      {renderFB()}
      {renderEvents()}
    </StyledFlex>
  );
};

export default ArtistOverview;
