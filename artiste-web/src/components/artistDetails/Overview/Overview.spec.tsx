import React from "react";
import { create } from "react-test-renderer";
import ArtistOverview from ".";

describe("ArtistOverview component", () => {
  test("Matches the snapshot without label", () => {
    const overview = create(<ArtistOverview artistName="test" />);
    expect(overview.toJSON()).toMatchSnapshot();
  });
});
