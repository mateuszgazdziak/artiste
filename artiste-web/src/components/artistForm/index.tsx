import React, { useRef, useState } from "react";
import styled from "styled-components";
import { withRouter, RouteComponentProps } from "react-router-dom";
import theme from "ui/theme";

const Form = styled.form`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-self: center;
`;

const Input = styled.input`
  padding: 4px;
  height: 64px;
  color: #fff;
  font-size: 25px;
  background: none;
  border: none;
  border-radius: 2px;
  align-self: flex-start;

  &:focus {
    outline: none;
  }
`;

const Button = styled.button`
  padding: 4px;
  margin: 2px;
  font-size: 16px;
  font-weight: 600;
  height: 64px;
  align-self: flex-end;
  color: ${theme.text.default};
  border-radius: 2px;
  background: none;
  cursor: pointer;
  border: none;
  background-color: ${theme.bg.reverse};

  &:hover {
    background: ${theme.bg.primary};
  }

  &:focus {
    box-shadow: 0 0 0 2px ${theme.bg.default}, 0 0 0 4px ${theme.bg.secondary};
    transition: box-shadow 0.2s ease-in-out;
  }
`;

const ArtistForm: React.FC<RouteComponentProps> = props => {
  const [showIsEmptyMessage, setShowIsEmptyMessage] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setShowIsEmptyMessage(false);
    const artistName = (inputRef.current && inputRef.current.value) || "";
    if (artistName) {
      props.history.push(`/artist/${artistName}`);
    } else {
      setShowIsEmptyMessage(true);
    }
  };

  return (
    <Form onSubmit={onSubmit}>
      <Input
        ref={inputRef}
        required
        placeholder="type artist here"
        onMouseEnter={() => {
          if (inputRef.current) {
            inputRef.current.focus();
          }
        }}
      />
      <Button>search</Button>
      {showIsEmptyMessage && <p>hahahha</p>}
    </Form>
  );
};

export default withRouter(ArtistForm);
