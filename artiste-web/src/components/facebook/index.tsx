import React from "react";
import { ReactComponent as FB } from "./facebook.svg";

const Facebook: React.FC = () => <FB />;

export default Facebook;