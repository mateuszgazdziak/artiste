import React from "react";
import { create } from "react-test-renderer";
import Facebook from ".";

describe("Facebook component", () => {
  test("Matches the snapshot without label", () => {
    const facebook = create(<Facebook  />);
    expect(facebook.toJSON()).toMatchSnapshot();
  });

});
