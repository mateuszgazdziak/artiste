import React from "react";
import styled from "styled-components";
import { ReactComponent as Logo } from "./logo.svg";

const StyledNav = styled.nav`
  display:flex;
  top: 10px;
  left: 0;
  right: 0;
  background-color:black;
  svg {
    height: 45px;
    vertical-align: middle;
  }

`;

const NavBar: React.FC = () => (
  <StyledNav>
    <a href={"/"} className="logo">
      <Logo />
    </a>
  </StyledNav>
);

export default NavBar;
