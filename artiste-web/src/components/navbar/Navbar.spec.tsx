import React from "react";
import { create } from "react-test-renderer";
import NavBar from ".";

describe("Header component", () => {
  test("Matches the snapshot", () => {
    const header = create(<NavBar />);
    expect(header.toJSON()).toMatchSnapshot();
  });
});
