import React from "react";
import { create } from "react-test-renderer";
import Footer from ".";

describe("Footer component", () => {
  test("Matches the snapshot without label", () => {
    const footer = create(<Footer />);
    expect(footer.toJSON()).toMatchSnapshot();
  });
});
