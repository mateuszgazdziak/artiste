import React from "react";
import Emoji from "components/emoji";
import { StyledFooter } from "./style";

const Footer: React.FC = () => (
  <StyledFooter>
    <a href="https://www.bandsintown.com/">via bandsintown API</a>
    <Emoji symbol="❤" />
  </StyledFooter>
);

export default Footer;
