import React from "react";

type ErrorBoundaryState = {
  error: any;
};

export class ErrorBoundary extends React.Component<{}, ErrorBoundaryState> {
  state = { error: null };

  static getDerivedStateFromError(error: any) {
    return { hasError: true };
  }

  componentDidCatch(error: any) {
    this.setState({ error });
  }

  render() {
    if (this.state.error) {
      return <h1>Something went wrong</h1>;
    }

    return this.props.children;
  }
}
