import React from "react";
import { create } from "react-test-renderer";
import Loader from ".";

describe("Emoji component", () => {
  test("Matches the snapshot without label", () => {
    const loader = create(<Loader />);
    expect(loader.toJSON()).toMatchSnapshot();
  });
});
