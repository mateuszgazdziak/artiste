import React from "react";
import { ReactComponent as LoaderSvg } from "img/loader.svg";

const Loader: React.FC = () => {
  return (
    <div>
      <LoaderSvg />
    </div>
  );
};

export default Loader;
