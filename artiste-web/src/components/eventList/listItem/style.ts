import styled from "styled-components";

export const Item = styled.li`
  border-bottom: 1px solid ${({ theme }) => theme.bg.secondary};
  padding-bottom: 12px;
  padding-top: 12px;

  &:last-child {
    border-bottom: none;
  }
`;


