import React from "react";
import { Event } from "../../../store/models";
import { Item } from "./style";

const ListItem: React.FC<Event> = (props: Event) => {
  const { eventVenue, eventCountry, eventDate } = props;
  return (
    <Item>
      Date: {eventDate.slice(0, 10)} <br />
      Venue: {eventVenue} <br />
      Country: {eventCountry} <br />
    </Item>
  );
};

export default ListItem;
