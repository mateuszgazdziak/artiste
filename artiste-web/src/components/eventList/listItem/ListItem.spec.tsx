import React from "react";
import ListItem from "./";
import { create } from "react-test-renderer";
import { Event } from "../../../store/models";
import { ThemeProvider } from "styled-components";
import theme from "ui/theme";

describe("ListItem component", () => {
  test("Matches the snapshot", () => {
    const event: Event = {
      eventCountry: "USA",
      eventDate: "2019-08-10",
      eventVenue: "Madison Square Garden"
    };

    const item = create(
      <ThemeProvider theme={theme}>
        <ListItem {...event} />
      </ThemeProvider>
    );
    expect(item.toJSON()).toMatchSnapshot();
  });
});
