import React from "react";
import { create } from "react-test-renderer";
import EventList from ".";
import theme from "ui/theme";
import { ThemeProvider } from "styled-components";

describe("EventList component", () => {
  const events = [
    {
      eventVenue: "test",
      eventCountry: "test",
      eventDate: "test"
    },
    {
      eventVenue: "test2",
      eventCountry: "test2",
      eventDate: "test2"
    }
  ];

  test("Matches the snapshot without label", () => {
    const eventList = create(
      <ThemeProvider theme={theme}>
        <EventList events={events} />
      </ThemeProvider>
    );
    expect(eventList.toJSON()).toMatchSnapshot();
  });
});
