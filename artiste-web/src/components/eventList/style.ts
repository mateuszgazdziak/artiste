import styled from "styled-components";

export const ListWrapper = styled.ul`
  background-color: ${({ theme }) => theme.bg.reverse};
  border-radius: 4px;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  padding: 0;
  flex-direction: column;
  list-style: none;
`;
