import React from "react";
import ListItem from "components/eventList/listItem";
import { ListWrapper } from "./style";
import { Event } from "../../store/models";

type Props = {
  events: Event[];
};

const EventList: React.FC<Props> = ({ events }) => (
  <ListWrapper>
    {events.map(event => (
      <ListItem key={event.eventDate} {...event} />
    ))}
  </ListWrapper>
);

export default EventList;
