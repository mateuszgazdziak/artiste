import apiService, { API_ROOT } from "api/apiServcie";

class Api {
  getArtistDetails(name: string) {
    return apiService.get(`${API_ROOT}/artist/${name}`);
  }
}

export const apiCall = new Api();
