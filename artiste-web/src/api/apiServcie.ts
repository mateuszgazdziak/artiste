import axios, { AxiosInstance } from "axios";

export const SERVER_ROOT = process.env.REACT_APP_URL || "localhost:3001";

export const API_ROOT = `http://${SERVER_ROOT}`;
const TIMEOUT = 20000;
const HEADERS = {
  Accept: "application/json",
  "Content-Type": "application/json"
};

const handleSuccess = (response: any) => response;
const handleError = (error: Error) => Promise.reject(error);

class ApiService {
  private client: AxiosInstance;
  constructor() {
    this.client = axios.create({
      baseURL: API_ROOT,
      headers: HEADERS,
      timeout: TIMEOUT
    });

    this.client.interceptors.response.use(handleSuccess, handleError);
  }

  get(path: string) {
    return this.client.get(path).then((response: any) => response.data);
  }
  post(path: string, payload: object) {
    return this.client
      .post(path, payload)
      .then((response: any) => response.data);
  }
  put(path: string, payload: object) {
    return this.client
      .put(path, payload)
      .then((response: any) => response.data);
  }
  patch(path: string, payload: object) {
    return this.client
      .patch(path, payload)
      .then((response: any) => response.data);
  }
  delete(path: string, payload: object) {
    return this.client
      .delete(path, payload)
      .then((response: any) => response.data);
  }
}

const apiService = new ApiService();

export default apiService;
