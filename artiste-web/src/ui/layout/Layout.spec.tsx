import React from "react";
import { MainFlex, StyledMainFlex } from "./";
import { create } from "react-test-renderer";

describe("MainFlex component", () => {
  test("Matches the snapshot without label", () => {
    const flex = create(<MainFlex />);
    expect(flex.toJSON()).toMatchSnapshot();
  });

  test("StyledMainFlex the snapshot with label", () => {
    const flex = create(<StyledMainFlex />);
    expect(flex.toJSON()).toMatchSnapshot();
  });
});
