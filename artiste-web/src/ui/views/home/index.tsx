import React from "react";
import { StyledFlex } from "ui/views/style";
import ArtistForm from "components/artistForm";

const Home: React.FC = () => (
  <StyledFlex>
    <ArtistForm />
  </StyledFlex>
);

export default Home;
