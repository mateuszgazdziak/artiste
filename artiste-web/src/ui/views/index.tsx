import React from "react";
import { StyledMainFlex } from "ui/layout";
import NavBar from "components/navbar";
import Home from "ui/views/home";
import Details from "ui/views/details";
import { RouteComponentProps } from "react-router";
import { Wrapper } from "./style";
import Footer from "components/footer";
import { ErrorBoundary } from "components/error/ErrorBaundry";

const Views: React.FC<RouteComponentProps> = props => {
  const renderView = () => {
    switch (props.match.path) {
      case "/artist/:artistName":
        return (
          <ErrorBoundary>
            <Details />
          </ErrorBoundary>
        );
      case "/home":
      case "/about":
      default: {
        return (
          <ErrorBoundary>
            <Home />
          </ErrorBoundary>
        );
      }
    }
  };

  return (
    <Wrapper>
      <NavBar />
      <StyledMainFlex>{renderView()}</StyledMainFlex>
      <Footer />
    </Wrapper>
  );
};

export default Views;
