import styled from "styled-components";
import theme from "ui/theme";
import { FlexCol } from "components/globals";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  color: ${theme.text.default};
  text-align: center;
  min-height: 100vh;
`;

export const StyledFlex = styled(FlexCol)`
  display: flex;
  flex-direction: column;
  color: ${theme.text.default};
  text-align: center;
`;
