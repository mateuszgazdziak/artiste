import React from "react";
import { StyledFlex } from "ui/views/style";
import ArtistDetails from "components/artistDetails";

const Details: React.FC = () => (
  <StyledFlex>
    <ArtistDetails />
  </StyledFlex>
);

export default Details;
