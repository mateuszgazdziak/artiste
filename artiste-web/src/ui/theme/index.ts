export default {
  bg: {
    default: "#FFFFFF",
    reverse: "#16171A",
    primary: "#10e0b7",
    secondary: "#000",
  },
  text: {
    default2: "#24292E",
    default: "#FFF",
    secondary: "#10e0b7",
    alt: "#67717A",
    placeholder: "#7C8894"
  },
  space: {
    default: "#0062D6",
    alt: "#1CD2F2",
    wash: "#E5F0FF",
    border: "#BDD8FF",
    dark: "#0F015E"
  },
  paddings: {
    small: "12px",
    normal: "24px",
    large: "32px"
  },
  brand: {
    default: "#708c8f",
    alt: "#3fb4c0",
    wash: "#E8E5FF",
    border: "#DDD9FF",
    dark: "#2A0080"
  }
};
