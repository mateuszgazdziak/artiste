import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import consfigureStore from "store";
import Routes from "routes";

const store = consfigureStore();

const App: React.FC = () => {

  return (
    <Provider store={store}>
      <Router>
        <Routes />
      </Router>
    </Provider>
  );
};

export default App;
