import React from "react";
import { Route, Switch, RouteComponentProps, withRouter } from "react-router";
import { ThemeProvider } from "styled-components";
import theme from "ui/theme";
import Views from "ui/views";

const Routes: React.FC<RouteComponentProps> = props => {
  return (
    <ThemeProvider theme={theme}>
      <Switch>
        <Route exact path="/" component={Views} {...props} />
        <Route path="/artist/:artistName" component={Views} {...props} />
      </Switch>
    </ThemeProvider>
  );
};

export default withRouter(Routes);
