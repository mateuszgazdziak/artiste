
## Description

[Artsite AWS](http://ec2-3-16-112-144.us-east-2.compute.amazonaws.com:8000/) Running EC2 instance on AWS.

Backend Nest.js app is used as proxy and chache for web application.

## Running the app

```bash
# development
$ cd docker & docker-compose up -d
```
## Tools

### Frontend
 - TypeScript
 - Styled Components
 - Redux
 - React

 ### Beckend
 - TypeScript
 - Nest